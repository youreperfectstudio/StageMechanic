# StageMechanic

## About / Downloads

We are creating a game engine that allows one to play, create, and share 3D-tile/block based games.

StageMechanic is currently in Early Access and is available for PC and web through [GameJolt](https://gamejolt.com/games/StageMechanic/357305).

StageMechanic for Android is available [in the Google Play Store](https://play.google.com/store/apps/developer?id=You%27re+Perfect+Studio)


## Target audences

### Education and Makers Movement

We include a built-in level editor that acts as the star of the show.

This project is being developed as a teaching aid for beginning game development students - ages 5 and up including adults. A primary goal of the project is to fill a gap between game creation tools like [Bloxels](http://edu.bloxelsbuilder.com/) and more advanced tools like [Game Maker Studio](https://www.yoyogames.com/gamemaker). 

With StageMechanic you are not just playing a game someone else made - you have the opportunity to create your own game and to learn from and build on games that others create.

For the casual aspiring creator, we offer a range of pre-made blocks and characters with different mechanics and creator-defined properties.

Intermediate level creators can use the [built-in Lua interpreter and code editor](https://www.youtube.com/watch?v=FvleCxA1DH8&list=PLANow1r53Tu5EiuWOlDKUoO6J7Ul8WRAf) to modify all of the built in blocks, items, and characters and even create their own original mechanics. In the future users will also be able to easily import their own 3D models create in tools like [MagicaVoxel](https://ephtracy.github.io/) and [Tinkercad](https://www.tinkercad.com/#/) as well as create their own music, sound effects, etc.

Advanced users have full acess to the project's C# source code, allowing for infinite custamizability. As a professionally engineered and developed non-trivial project, StageMechanic offers a unique chance for students to move beyond tutorials and get hands on experience in a complex real-world game.

### AI Researchers

Future versions of StageMechanic will include both the Encog neural network library and TensorFlow accessable via the built-in Lua interpreter and code editor, allowing real-time AI experimentation.

### Block-pushing game fans

Fans of games such as [Sokoban](https://en.wikipedia.org/wiki/Sokoban), [Catherine](http://catherinethegame.com/fullbody/home.html), [Robo5](https://play.google.com/store/apps/details?id=com.animoca.google.robo5&hl=en_US), and [Pushmo](https://www.nintendo.com/games/detail/pushmo-3ds) will find pre-made blocks and items with mechanics inspired by these games - all fully customizable and reprogrammable. 

### 2D tile-based game-creation tool users

Users of game-creation tools like [Super Mario Maker](http://supermariomaker.nintendo.com/) and [Bloxels](http://edu.bloxelsbuilder.com/) will find many of the same concepts in StageMechanic. You can create your own levels using the built-in level editor, play your own levels, share your levels, and play levels created by others.

Unlike these games, StageMechanic is in 3D space and includes the possibility to script custom game mechanics - allowing for much more complex games. The built-in block and item types helps keep things simple for those just getting started.

### Game Developers

StageMechanic is [BSD licensed](https://opensource.org/licenses/BSD-3-Clause) allowing anyone to create their own open-source or commercial games based on our engine. See below for more information on licensing. 

## Licensing

This project is being developed as an open-source project entirely by volunteers. The resulting application is made freely available to users and all source code made freely available to developers/artists/etc.

We are using the BSD 3-clause license. This allows maximum freedom while protecting contributors. Essentially anyone is allowed to do whatever they want to with anything published as part of this project. 

Please note that some individual assets may be under other, compatible licenses inclyding CC0, CC-BY, or the Unity Store license. Please see in-game credits, asset filenames, in-folder license files, and the LICENSE-EXCEPTIONS file for additional information.

## Contribting

This project is being developed entirely by volunteers. If there is anything you would like to work on please [join our Discord chat](https://discord.gg/TBU4MyE) and let us know. Pretty much no matter what it is we can find a way to fit it in. Our project manager often says "do what you want, when you want, for as long as you want" and applies this not only to life but to project management.

### Tools/technologies used

* Game Engine: Unity (Latest Version)
* Primary Scripting Language: C# 7.0
* 3D Modeling: Blender
* 2D Painting: Photoshop, Affinity Designer, Clip Studio Paint, Filter Forge, Substance Designer/Painter, etc.

Note: Contributors may use other tools that work with the common formats so long as they have appropriate licenses to use those tools for creating contributions to a BSD+CC0 licensed project. For example the Student version of Z-Brush is not compatible because it only allows educational use.

### Getting Started

Step 1: Play at least one of the following games
* [Sokoban](https://en.wikipedia.org/wiki/Sokoban)
* [Catherine](http://catherine.wikia.com/wiki/Catherine_Wiki), published by Atlus
* [Robo5](https://play.google.com/store/apps/details?id=com.animoca.google.robo5&hl=en), published by Animoca
* [Pushmo](https://www.nintendo.com/games/detail/pushmo-3ds), published by Nintendo
* [Bloxels](https://www.bloxelsbuilder.com/), published by PixelPress
* [Super Mario Maker](http://supermariomaker.nintendo.com/)

Step 2: Join our Discord Chat: https://discord.gg/w7y8u5g

Step 3: Download and install the latest version on Unity Hub: https://unity3d.com/

Step 4: Install Github Desktop (or other Git client)