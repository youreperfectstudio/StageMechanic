﻿/*  
 * Copyright (C) 2018 You're Perfect Studio. All rights reserved.  
 * Licensed under the BSD 3-Clause License.
 * See LICENSE file in the project root for full license information.
 * See CONTRIBUTORS file in the project root for full list of contributors.
 */
using DG.Tweening;
using System.Collections;
using UnityEngine;

public class CameraController : MonoBehaviour
{

    public GameObject Cursor;

    public Vector3 offset;
    public bool LazyScroll = false;
    private float _cursorZoom = 0f;
    private float _playerZoom = 0f;
    public float zoom
    {
        get
        {
            if (BlockManager.PlayMode)
                return _playerZoom;
            return _cursorZoom;
        }
        set
        {
            if (BlockManager.PlayMode)
                _playerZoom = value;
            else
                _cursorZoom = value;
        }
    }

    public void RotateAroundPlatform(Vector3 direction)
    {
        if(direction == Vector3.left)
        {
            offset = new Vector3(10, Cursor.transform.position.y, 5);
        }
        else if (direction == Vector3.right)
        {
            offset = new Vector3(5, Cursor.transform.position.y, 10);
        }
		else if(direction == Vector3.up)
		{
			offset = new Vector3(0, Cursor.transform.position.y+10, 0);
		}
        if (BlockManager.ActiveBlock != null)
            transform.LookAt(BlockManager.ActiveBlock.Position);
        else
            transform.LookAt(new Vector3(BlockManager.ActiveFloor.transform.position.x,0, BlockManager.ActiveFloor.transform.position.z));
    }


    public void ResetZoom()
    {
        zoom = 0;
    }

    // Use this for initialization
    void Start()
    {
        offset = transform.position - Cursor.transform.position;
    }

    // Update is called once per frame
    void LateUpdate()
    {
        if(BlockManager.PlayMode)
        {
			Vector3 player1pos = PlayerManager.Player1Location();

			if (UIManager.MinimizePanning)
			{
				if (player1pos.IsValid())
				{
					//TODO make a function to calculate these instead of hardcoding values
					float xPos = 0f;
					if (player1pos.x > 36f)
						xPos = 38f;
					else if (player1pos.x > 30f)
						xPos = 32f;
					else if (player1pos.x > 24f)
						xPos = 26f;
					else if (player1pos.x > 18f)
						xPos = 20f;
					else if (player1pos.x > 12f)
						xPos = 14f;
					else if (player1pos.x > 6f)
						xPos = 8f;
					else if (player1pos.x < -36f)
						xPos = -38f;
					else if (player1pos.x < -30f)
						xPos = -32f;
					else if (player1pos.x < -24f)
						xPos = -26f;
					else if (player1pos.x < -18f)
						xPos = -20f;
					else if (player1pos.x < -12f)
						xPos = -14f;
					else if (player1pos.x < -6f)
						xPos = -8f;

					if (player1pos.IsValid())
						transform.DOMove(new Vector3(xPos, player1pos.y + 3f, player1pos.z - 7f + zoom), 0.2f);
				}
			}
			else
			{
				if (player1pos.IsValid())
					transform.DOMove(new Vector3(player1pos.x, player1pos.y + 3f, player1pos.z - 7f + zoom), 0.2f);
			}

		}
		else {
			if (Cursor.transform.position.IsValid())
				transform.DOMove(new Vector3(Cursor.transform.position.x + offset.x, Cursor.transform.position.y + offset.y, Cursor.transform.position.z - 7f + zoom), 0.2f);
        }
    }
}
