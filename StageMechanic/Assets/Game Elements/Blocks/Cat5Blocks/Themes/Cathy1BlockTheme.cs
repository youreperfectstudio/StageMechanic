﻿using UnityEngine;

[CreateAssetMenu(fileName ="Cathy1BlockTheme",menuName ="Cathy1BlockTheme")]
public class Cathy1BlockTheme : AbstractBlockTheme
{
	[Header("Normal Block")]
	public GameObject BasicBlock1;
	public GameObject BasicBlock2; //Optional
	public GameObject BasicBlock3; //Optional
	public GameObject BasicBlock4; //Optional
	public Sprite BasicBlockIcon;  //Optional

	[Header("Small Bomb")]
	public GameObject SmallBombIdle;
	public GameObject SmallBombTriggered; //Optional
	public ParticleSystem SmallBombExplosion; //Optional
	public Vector3 SmallBombExplosionOffset; //Optional
	public AudioClip SmallBombFuseSound; //Optional
	public AudioClip SmallBombExplosionSound; //Optional
	public Sprite SmallBombIcon; //Optional

	[Header("Large Bomb")]
	public GameObject LargeBombIdle;
	public GameObject LargeBombTriggered; //Optional
	public ParticleSystem LargeBombExplosion; //Optional
	public Vector3 LargeBombExplosionOffset; //Optional
	public AudioClip LargeBombFuseSound; //Optional
	public AudioClip LargeBombExplosionSound; //Optional
	public Sprite LargeBombIcon; //Optional

	[Header("Cracked")]
	public GameObject LightCracks;
	public GameObject HeavyCracks;
	public GameObject DisintigrationDust; //Optional
	public Vector3 DustOffset; //Optional
	public AudioClip CrackSound; //Optional
	public AudioClip DisintigrateSound; //Optional
	public Sprite LightCracksIcon; //Optional
	public Sprite HeavyCracksIcon; //Optional

	[Header("Grouped Blocks")]
	//TODO
	public Sprite GroupedBlockIcon; //Optional

	[Header("Goal")]
	public GameObject IdleGoal;
	public GameObject ActiveGoal; //Optional
	public ParticleSystem IdleGoalEffect; //Optional
	public ParticleSystem ActiveGoalEffect; //Optional
	public Vector3 GoalEffectsOffset; //Optional
	public AudioClip GoalSound; //Optional
	public Sprite GoalIcon; //Optional

	[Header("Heavy")]
	public GameObject Heavy;
	public Sprite HeavyIcon; //Optional

	[Header("Ice")]
	public GameObject Ice;
	public ParticleSystem RandomIceEffect;  //Optional
	public Vector3 IceEffectOffset;
	public Sprite IceIcon; //Optional

	[Header("Immobile")]
	public GameObject Immobile;
	public Sprite ImmobileIcon; //Optional

	[Header("Laser")]
	public GameObject IdleLaser;
	public GameObject ActiveLaser; //Optional
	public GameObject DisabledLaser; //Optional
	public ParticleSystem LaserEffect; //Optional
	public Vector3 LaserEffectOffset; //Optional
	public AudioClip LaserWarningSound; //Optional
	public AudioClip LaserFireSound; //Optional (note: I'm afirin' ma layzar)
	public AudioClip DisablingLaserSound; //Optional
	public Sprite LaserIcon; //Optional

	[Header("Monster")]
	public GameObject ActiveMonster;
	public GameObject MovingMonster; //Optional
	public GameObject DisarmedMonster; //Optional (default: BasicBlock1)
	public AudioClip RandomMonsterSound; //Optional
	public AudioClip MoveMonsterSound; //Optional
	public AudioClip DisarmMonsterSound; //Optional
	public ParticleSystem RandomMonsterEffect; //Optional
	public ParticleSystem MoveMonsterEffect; //Optional
	public ParticleSystem DisarmMonsterEffect; //Optional
	public Vector3 MonsterEffectOffset; //Optional
	public Sprite MonsterIcon; //Optional

	[Header("Mystery")]
	public GameObject IdleMystery;
	public GameObject RevealingMystery; //Optional
	public AudioClip MysteryRevealSound; //Optional
	public Sprite MysteryIcon; //Optional

	[Header("spike Trap")]
	public GameObject TrapArmed;
	public GameObject TrapDisarmed;
	public GameObject TrapWarning; //Optional
	public GameObject TrapActive; //Optional
	public ParticleSystem TrapRandomEffect; //Optional
	public ParticleSystem TrapActiveEffect; //Optional
	public Vector3 TrapEffectOffset; //Optional
	public AudioClip TrapWarningSound; //Optional
	public AudioClip TrapActivatedSound; //Optional
	public Sprite TrapIcon; //Optional

	[Header("Spring")]
	public GameObject IdleSpring;
	public GameObject ActiveSpring; //Optional
	public AudioClip SpringSound; //Optional
	public ParticleSystem SpringEffect; //Optional
	public Vector3 SpringEffectOffset; //Optional
	public Sprite SpringIcon; //Optional

	[Header("Vortex")]
	public GameObject IdleVortex;
	public GameObject ActiveVortex; //Optional
	public AudioClip ActiveVortexSound; //Optional
	public ParticleSystem RandomVortexEffect; //Optional
	public ParticleSystem ActiveVortexEffect; //Optional
	public Vector3 VortexEffectOffset; //Optional
	public Sprite VortexIcon; //Optional

	[Header("Teleport")]
	public GameObject IdleTeleport;
	public GameObject ActiveTeleport; //Optional
	public ParticleSystem IdleTeleportEffect; //Optional
	public ParticleSystem ActiveTeleportEffect; //Optional
	public Vector3 TeleportEffectsOffset; //Optional
	public AudioClip TeleportSound; //Optional
	public Sprite TeleportIcon; //Optional

}
