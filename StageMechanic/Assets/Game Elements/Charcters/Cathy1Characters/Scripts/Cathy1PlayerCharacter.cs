﻿/*  
 * Copyright (C) 2018 You're Perfect Studio. All rights reserved.  
 * Licensed under the BSD 3-Clause License.
 * See LICENSE file in the project root for full license information.
 * See CONTRIBUTORS file in the project root for full list of contributors.
 */
using DG.Tweening;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cathy1PlayerCharacter : AbstractPlayerCharacter
{

	public GameObject Player1Prefab;
	public RuntimeAnimatorController Player1AnimationController;
	public Avatar Player1Avatar;

	public AudioClip WalkSound;
	public AudioClip JumpSound;
	public AudioClip LandSound;
	public AudioClip DieSound;
	public AudioClip ThudSound;
	public AudioClip GameOverSound;

	public AudioClip[] EffortSounds;

	private GameObject _player;

	private const float HEIGHT_ADJUST = 0.5f;
	public float WalkTime { get; set; } = 0.15f;
	public float Granularity { get; set; } = 1.0f;
	public float ClimbSpeed { get; set; } = 0.05f;
	public int MaxClimbHeight { get; set; } = 1;

	public int HighestStepChain = 0;
	private float _highestPosition = float.NaN;
	public float HighestPosition
	{
		get
		{
			return _highestPosition;
		}
		set
		{
			if (float.IsNaN(_highestPosition))
				_highestPosition = value;
			float newHighest = Math.Max(_highestPosition, value);
			if (newHighest == _highestPosition)
				return;
			int steps = (int)Math.Round(newHighest - _highestPosition);
			Score += ((10 * (steps + 1) * (steps / 2)) + (Math.Min(30, ++HighestStepChain) * 10));
			_highestPosition = newHighest;

		}
	}

	private static System.Random rng = new System.Random();

	public override Vector3 FacingDirection
	{
		get
		{
			return _facingDirection;
		}
		set
		{
			if (_player == null)
				StartCoroutine(UpdateFacingDirectionLater(value));
			else
				Face(value);
		}
	}

	public enum State
	{
		Idle = 0,
		Walk,
		Aproach,
		Climb,
		Center,
		Sidle,
		SidleMove,
		Slide,
		PushPull,
		ActivateItem,
		TakeDamage,
		Fall,
		Win,
		Loose,
		Tie
	}
	private State _currentState = State.Idle;
	public State CurrentMoveState
	{
		get
		{
			return _currentState;
		}
		set
		{
			if (_player == null)
			{
				StartCoroutine(UpdateStateLater(value));
			}
			else
			{
				_currentState = value;
				if (value == State.Sidle || value == State.SidleMove || value == State.Fall)
				{
					_player.transform.position = transform.position - new Vector3(0f, HEIGHT_ADJUST, 0f) + new Vector3(FacingDirection.x / 3f, 0.2f, FacingDirection.z / 3f);
				}
				else
				{
					_player.transform.position = transform.position - new Vector3(0f, HEIGHT_ADJUST, 0f);
				}
				(CurrentBlock as AbstractBlock)?.OnPlayerMovement(this, PlayerMovementEvent.EventType.Stay);
			}
		}
	}

	public IBlock CurrentBlock
	{
		get
		{
			if (CurrentMoveState == State.Sidle || CurrentMoveState == State.SidleMove)
				return BlockManager.GetBlockNear(Position + FacingDirection);
			return BlockManager.GetBlockNear(Position + Vector3.down);
		}
	}

	public IEnumerator UpdateStateLater(State state)
	{
		while (_player == null)
			yield return new WaitForEndOfFrame();
		CurrentMoveState = state;
	}

	public IEnumerator UpdateFacingDirectionLater(Vector3 direction)
	{
		while (_player == null)
			yield return new WaitForEndOfFrame();
		FacingDirection = direction;
	}

	public void PlayDieSound()
	{
		AudioEffectsManager.PlaySound(DieSound);
	}

	public void PlayGameOverSound()
	{
		AudioEffectsManager.PlaySound(GameOverSound);
	}

	public void PlayThudSound()
	{
		AudioEffectsManager.PlaySound(ThudSound);
	}

	public GameObject Character { get; set; }
	public bool IsWalking { get; set; } = false;

	public Vector3 CurrentLocation
	{
		get
		{
			return transform.localPosition;
		}
		set
		{
			transform.localPosition = value;
		}
	}

	public override List<string> StateNames
	{
		get
		{
			List<string> states = new List<string>();
			foreach (State state in Enum.GetValues(typeof(State)))
				states.Add(state.ToString());
			return states;
		}
	}

	public override int CurrentStateIndex
	{
		get
		{
			return StateNames.IndexOf(CurrentMoveState.ToString());
		}
		set
		{
			Debug.Assert(StateNames.Count > value);
			string stateName = StateNames[value];
			//TODO check this
			State state = (State)Enum.Parse(typeof(State), stateName);
			CurrentMoveState = state;
		}
	}

	public void ForceMove(Vector3 offset)
	{
		if (offset != Vector3.zero)
			CurrentLocation += offset;
	}

	public void Teleport(Vector3 location)
	{
		if (CurrentLocation != location)
			CurrentLocation = location;
	}

	public void SlideForward()
	{
		if (BlockManager.GetBlockAt(transform.position + FacingDirection) != null)
			return;

		Vector3 location = CurrentLocation + FacingDirection;
		IBlock oldBlock = CurrentBlock;
		transform.DOMove(location, WalkTime)
			.OnStart(SlideStarted)
			.OnComplete(() => SlideComplete(oldBlock));
	}

	private void SlideStarted()
	{
		CurrentMoveState = State.Slide;
		_player.GetComponent<Animator>().SetBool("sliding", true);
		//TODO slide sound
	}

	private void SlideComplete(IBlock oldBlock)
	{
		_player.GetComponent<Animator>().SetBool("sliding", false);
		(CurrentBlock as AbstractBlock)?.OnPlayerMovement(this, PlayerMovementEvent.EventType.Enter);
		AbstractBlock oab = (oldBlock as AbstractBlock);
		if (oab != null && oab.gameObject != null)
			oab.OnPlayerMovement(this, PlayerMovementEvent.EventType.Leave);
		CurrentMoveState = State.Idle;
	}


	public void Walk(Vector3 direction)
	{
		Vector3 location = CurrentLocation + direction;
		IBlock oldBlock = CurrentBlock;
		transform.DOMove(location, WalkTime)
			.OnStart(WalkStarted)
			.OnComplete(() => WalkComplete(oldBlock));
	}

	private void WalkStarted()
	{
		CurrentMoveState = State.Walk;
		_player.GetComponent<Animator>().SetBool("walking", true);
		AudioEffectsManager.PlaySound(WalkSound);
	}

	private void WalkComplete(IBlock oldBlock)
	{
		_player.GetComponent<Animator>().SetBool("walking", false);
		(CurrentBlock as AbstractBlock)?.OnPlayerMovement(this, PlayerMovementEvent.EventType.Enter);
		AbstractBlock oab = (oldBlock as AbstractBlock);
		if (oab != null && oab.gameObject != null)
			oab.OnPlayerMovement(this, PlayerMovementEvent.EventType.Leave);
		CurrentMoveState = State.Idle;
	}

	public void DoPushPull(Vector3 direction)
	{
		Vector3 location = CurrentLocation + direction;
		IBlock oldBlock = CurrentBlock;
		transform.DOMove(location, WalkTime)
			.OnStart(PushPullStarted)
			.OnComplete(() => PushPullComplete(oldBlock));
	}

	private void PushPullStarted()
	{
		CurrentMoveState = State.PushPull;
		_player.GetComponent<Animator>().SetBool("sidling", true);
		AudioEffectsManager.PlaySound(WalkSound);
	}

	private void PushPullComplete(IBlock oldBlock)
	{
		_player.GetComponent<Animator>().SetBool("sidling", false);
		(CurrentBlock as AbstractBlock)?.OnPlayerMovement(this, PlayerMovementEvent.EventType.Enter);
		AbstractBlock oab = (oldBlock as AbstractBlock);
		if (oab != null && oab.gameObject != null)
			oab.OnPlayerMovement(this, PlayerMovementEvent.EventType.Leave);
		CurrentMoveState = State.Idle;
	}

	public void Climb(Vector3 direction)
	{
		Vector3 origin = CurrentLocation;
		Vector3 location = CurrentLocation + direction;
		Vector3 offset = (location - CurrentLocation);
		IBlock oldBlock = CurrentBlock;
		_player.GetComponent<Animator>().SetBool("sidling", false);

		Sequence climbSequence = DOTween.Sequence();

		if (CurrentMoveState != State.Sidle && CurrentMoveState != State.SidleMove)
		{
			Vector3 firstPart = origin + new Vector3(offset.x / 4, 0f, offset.z / 4);
			float firstPartTime = WalkTime * 0.25f;
			climbSequence.Append(transform.DOMove(firstPart, firstPartTime)
				.OnStart(ApproachStarted)
				.OnComplete(ApproachCompete));
		}
		float secondPartTime = WalkTime * 0.75f;

		climbSequence.Append(transform.DOMove(location, secondPartTime)
			.OnStart(ClimbStarted)
			.OnComplete(() => ClimbComplete(oldBlock)));

	}

	private void ApproachStarted()
	{
		CurrentMoveState = State.Aproach;
		_player.GetComponent<Animator>().SetBool("walking", true);
		AudioEffectsManager.PlaySound(JumpSound);
	}

	private void ApproachCompete()
	{
		_player.GetComponent<Animator>().SetBool("walking", false);
	}

	private void ClimbStarted()
	{
		CurrentMoveState = State.Climb;
		_player.GetComponent<Animator>().SetBool("climbing", true);

	}

	private void ClimbComplete(IBlock oldBlock)
	{
		AudioEffectsManager.PlaySound(LandSound);
		AbstractBlock oab = oldBlock as AbstractBlock;
		if (oab != null && oab.gameObject != null)
			oab.OnPlayerMovement(this, PlayerMovementEvent.EventType.Leave);
		_player.GetComponent<Animator>().SetBool("climbing", false);
		_player.GetComponent<Animator>().SetBool("walking", false);
		CurrentMoveState = State.Idle;
		HighestPosition = transform.position.y;
		(CurrentBlock as AbstractBlock)?.OnPlayerMovement(this, PlayerMovementEvent.EventType.Enter);
	}

	public IEnumerator SidleTo(Vector3 location)
	{
		if (CurrentMoveState == State.Idle)
		{
			_player.GetComponent<Animator>().SetBool("sidling", true);
			CurrentMoveState = State.Aproach;
			yield return new WaitForEndOfFrame();
			CurrentMoveState = State.Climb;
			yield return new WaitForEndOfFrame();
			Teleport(location + Vector3.down);
			yield return new WaitForEndOfFrame();
			CurrentMoveState = State.Sidle;
			yield return new WaitForSeconds(0.1f);
			yield return null;
		}
		else
		{
			_player.GetComponent<Animator>().SetBool("sidleMoving", true);
			CurrentMoveState = State.SidleMove;
			yield return new WaitForEndOfFrame();
			float journey = 0f;
			Vector3 origin = CurrentLocation;

			Tween tween = transform.DOMove(location, WalkTime);
			yield return tween.WaitForCompletion();

			yield return new WaitForEndOfFrame();
			_player.GetComponent<Animator>().SetBool("sidleMoving", false);
			CurrentMoveState = State.Sidle;
			yield return null;
		}
	}

	public virtual void Sidle(Vector3 direction)
	{
		StartCoroutine(SidleTo(CurrentLocation + direction));
	}

	private IEnumerator BoingyTo(Vector3 location)
	{
		CurrentMoveState = State.Idle;
		_player.GetComponent<Animator>().SetBool("sidling", true);
		yield return new WaitForEndOfFrame();
		float journey = 0f;
		Vector3 origin = CurrentLocation;
		while (journey <= (WalkTime*4))
		{
			journey = journey + Time.deltaTime;
			float percent = Mathf.Clamp01(journey / (WalkTime*4));

			Teleport(Vector3.Lerp(origin, location, percent));
			AbstractBlock block = BlockManager.GetBlockNear(Utility.Round(Position, 1) + Vector3.up,0.25f);
			if ( block != null)
			{
				transform.position = block.Position + Vector3.down;
				break;
			}
			yield return null;
		}
		yield return new WaitForEndOfFrame();
		_player.GetComponent<Animator>().SetBool("sidling", false);
		CurrentMoveState = State.Fall;
		yield return null;
	}

	public void Boingy(Vector3 location)
	{
		StartCoroutine(BoingyTo(location));
	}


	// Use this for initialization
	void Start()
	{
		_player = Instantiate(Player1Prefab, transform.position - new Vector3(0f, HEIGHT_ADJUST, 0f), transform.rotation, gameObject.transform);
		_player.transform.RotateAround(transform.position, transform.up, 180f);
		_player.GetComponent<Animator>().runtimeAnimatorController = Player1AnimationController;
		_player.GetComponent<Animator>().avatar = Player1Avatar;
	}

	private void Update()
	{
		ApplyGravity();
		if (CurrentMoveState == State.Idle && BlockManager.GetBlockNear(transform.position, radius: 0.25f))
			TakeDamage(float.PositiveInfinity, "Squished");
	}

	public override bool TakeDamage(float unused, string alsoNotUsed)
	{

		if (!UIManager.IsSinglePlayerDeathDialogOpen)
		{
			UIManager.ShowSinglePlayerDeathDialog(DieSound);
		}
		return true;
	}

	public override bool ApplyGravity(float factor = 1f, float acceleration = 0f)
	{
		if (CurrentMoveState == State.Idle || CurrentMoveState == State.Fall)
		{
			if (BlockManager.GetBlockNear(transform.position + Vector3.down) == null)
			{
				CurrentMoveState = State.Fall;
				base.ApplyGravity(factor, acceleration);
				if (BlockManager.GetBlockAt(transform.position + FacingDirection) != null && BlockManager.GetBlockAt(transform.position + FacingDirection + Vector3.up) == null)
				{
					CurrentMoveState = State.Sidle;
				}
				return true;
			}
			else
			{
				_player.GetComponent<Animator>().SetBool("sidling", false);
				_player.GetComponent<Animator>().SetBool("walking", false);
				_player.GetComponent<Animator>().SetBool("sidleMoving", false);
				_player.GetComponent<Animator>().SetBool("sliding", false);
				CurrentMoveState = State.Idle;
				return false;
			}
		}
		if (CurrentMoveState == State.Sidle && BlockManager.GetBlockAt(transform.position + FacingDirection) == null)
		{
			CurrentMoveState = State.Fall;
			return false;
		}
		return false;
	}

	public override bool Face(Vector3 direction)
	{
		if (FacingDirection == direction)
			return false;
		float degrees = 0f;
		if (FacingDirection == Vector3.back)
		{
			if (direction == Vector3.left)
				degrees = 90f;
			else if (direction == Vector3.right)
				degrees = -90f;
			else if (direction == Vector3.forward)
				degrees = 180f;
		}
		else if (FacingDirection == Vector3.forward)
		{
			if (direction == Vector3.left)
				degrees = -90f;
			else if (direction == Vector3.right)
				degrees = 90f;
			else if (direction == Vector3.back)
				degrees = 180f;
		}
		else if (FacingDirection == Vector3.left)
		{
			if (direction == Vector3.forward)
				degrees = 90f;
			else if (direction == Vector3.right)
				degrees = -180f;
			else if (direction == Vector3.back)
				degrees = -90f;
		}
		else if (FacingDirection == Vector3.right)
		{
			if (direction == Vector3.left)
				degrees = 180f;
			else if (direction == Vector3.back)
				degrees = 90f;
			else if (direction == Vector3.forward)
				degrees = -90f;
		}
		_player.transform.RotateAround(transform.position, transform.up, degrees);
		base.FacingDirection = direction;
		return true;
	}

	public override bool TurnAround()
	{
		return Face(-FacingDirection);
	}

	public static Vector3 ReverseDirection(Vector3 direction)
	{
		if (direction == Vector3.left)
			return Vector3.right;
		else if (direction == Vector3.right)
			return Vector3.left;
		else if (direction == Vector3.forward)
			return Vector3.back;
		else if (direction == Vector3.back)
			return Vector3.forward;
		else
			return Vector3.zero;
	}

	public float QueueMove(Vector3 direction, bool pushpull = false)
	{
		float expectedTime = 0f;
		if (pushpull)
		{
			return PushPull(direction);
		}
		if (CurrentMoveState == State.Idle)
		{
			if (FacingDirection == direction || direction == Vector3.up || direction == Vector3.down)
			{
				IBlock blockInWay = BlockManager.GetBlockNear(transform.position + direction, radius: 0.25f);
				if (MaxClimbHeight >= 2 && blockInWay == null)
				{
					IBlock potentialBlock = BlockManager.GetBlockAt(transform.position + direction + Vector3.up);
					IBlock blockAbove = BlockManager.GetBlockAt(transform.position + Vector3.up);
					if (blockAbove == null && potentialBlock != null)
					{
						IBlock oneBlockUp = BlockManager.GetBlockAt(transform.position + direction + Vector3.up + Vector3.up);
						blockAbove = BlockManager.GetBlockAt(transform.position + Vector3.up + Vector3.up);
						if (oneBlockUp == null && blockAbove == null)
						{
							if (potentialBlock.MotionState == BlockMotionState.Grounded || potentialBlock.MotionState == BlockMotionState.Edged || potentialBlock.MotionState == BlockMotionState.GroupHover)
							{
								Climb(direction + Vector3.up + Vector3.up);
								return 0.35f;
							}
						}
					}
				}

				if (blockInWay != null)
				{
					IBlock oneBlockUp = BlockManager.GetBlockAt(transform.position + direction + Vector3.up);
					IBlock blockAbove = BlockManager.GetBlockAt(transform.position + Vector3.up);
					if (oneBlockUp == null && blockAbove == null)
					{
						if (blockInWay.MotionState == BlockMotionState.Grounded || blockInWay.MotionState == BlockMotionState.Edged || blockInWay.MotionState == BlockMotionState.GroupHover)
							Climb(direction + Vector3.up);
					}
					else if(MaxClimbHeight >= 2 && blockAbove == null)
					{
						oneBlockUp = BlockManager.GetBlockAt(transform.position + direction + Vector3.up + Vector3.up);
						blockAbove = BlockManager.GetBlockAt(transform.position + Vector3.up + Vector3.up);
						if (oneBlockUp == null && blockAbove == null)
						{
							if (blockInWay.MotionState == BlockMotionState.Grounded || blockInWay.MotionState == BlockMotionState.Edged || blockInWay.MotionState == BlockMotionState.GroupHover)
								Climb(direction + Vector3.up + Vector3.up);
						}
					}
				}
				else
				{
					IBlock nextFloor = BlockManager.GetBlockAt(transform.position + direction + Vector3.down);
					if (nextFloor != null)
					{
						if (nextFloor.MotionState == BlockMotionState.Grounded || nextFloor.MotionState == BlockMotionState.Edged || nextFloor.MotionState == BlockMotionState.GroupHover)
							Walk(direction);
						expectedTime = WalkTime;
					}
					else
					{
						IBlock stepDown = BlockManager.GetBlockAt(transform.position + direction + Vector3.down + Vector3.down);
						if (stepDown != null)
						{
							if (stepDown.MotionState == BlockMotionState.Grounded || stepDown.MotionState == BlockMotionState.Edged || stepDown.MotionState == BlockMotionState.GroupHover)
								Climb(direction + Vector3.down);
						}
						else
						{
							if (direction == FacingDirection)
								TurnAround();
							Sidle(direction);
						}
					}
				}
				expectedTime = 0.3f;
			}
			else
			{
				Face(direction);
				expectedTime = 0.2f;
			}
		}
		else if (CurrentMoveState == State.Sidle)
		{
			IBlock attemptedGrab = null;
			if (direction == Vector3.right || direction == Vector3.left)
			{
				Vector3 originalDirection = direction;
				if (FacingDirection == Vector3.forward) { }
				else if (FacingDirection == Vector3.back)
				{
					if (direction == Vector3.left)
						direction = Vector3.right;
					else
						direction = Vector3.left;
				}
				else if (FacingDirection == Vector3.right)
				{
					if (direction == Vector3.left)
						direction = Vector3.forward;
					else
						direction = Vector3.back;
				}
				else if (FacingDirection == Vector3.left)
				{
					if (direction == Vector3.left)
						direction = Vector3.back;
					else
						direction = Vector3.forward;
				}

				attemptedGrab = BlockManager.GetBlockAt(transform.position + direction);
				if (attemptedGrab == null)
				{
					attemptedGrab = BlockManager.GetBlockAt(transform.position + direction + FacingDirection);
					if (attemptedGrab != null)
					{
						if (BlockManager.GetBlockAt(transform.position + direction + Vector3.up) == null)
							Sidle(direction);
					}
					else
					{
						if (BlockManager.GetBlockAt(transform.position + direction + FacingDirection + Vector3.up) == null)
						{
							Sidle(FacingDirection + direction);
							if (originalDirection == Vector3.left)
								TurnRight();
							else
								TurnLeft();
						}
					}
					expectedTime = 0.35f;
				}
				else
				{
					Turn(originalDirection);
					expectedTime = 0.2f;
				}
			}
			else if (direction == Vector3.forward)
			{
				attemptedGrab = BlockManager.GetBlockAt(transform.position + Vector3.up + FacingDirection);
				if (attemptedGrab == null)
				{
					Climb(FacingDirection + Vector3.up);
					expectedTime = 0.35f;
				}
			}
			else if (direction == Vector3.back)
			{
				CurrentMoveState = State.Fall;
				expectedTime = 0.1f;
			}
		}
		return expectedTime;
	}

	public virtual float PushPull(Vector3 direction)
	{
		if (direction == Vector3.zero)
			return 0f;

		if (CurrentMoveState == State.Sidle)
		{
			if (BlockManager.GetBlockAt(transform.position + Vector3.down) == null)
				return 0f;
			CurrentMoveState = State.Fall;
		}


		if (direction != FacingDirection && direction != ReverseDirection(FacingDirection))
			return 0f;

		//Don't allow pull if there is a block in your way
		if (direction == ReverseDirection(FacingDirection))
		{
			IBlock blockInWay = BlockManager.GetBlockNear(transform.position + ReverseDirection(FacingDirection), radius: 0.25f);
			if (blockInWay != null)
				return 0f;
		}

		//TODO no sideways movement
		IBlock blockInQuestion = BlockManager.GetBlockAt(transform.position + FacingDirection);
		if (blockInQuestion == null)
			return 0f;
		//TODO make this one movement
		bool moved = false;
		Serializer.RecordUndo();
		if (direction == FacingDirection)
		{
			if (BlockManager.BlockGroupNumber(blockInQuestion) > -1)
				moved = BlockManager.PushGroup(BlockManager.BlockGroupNumber(blockInQuestion), direction, 1);
			else
				moved = blockInQuestion.Push(direction, 1);
		}
		else
		{
			if (BlockManager.BlockGroupNumber(blockInQuestion) > -1)
				moved = BlockManager.PullGroup(BlockManager.BlockGroupNumber(blockInQuestion), direction, 1);
			else
				moved = blockInQuestion.Pull(direction, 1);
		}
		if (moved)
		{
			if (EffortSounds.Length > 0)
			{
				int gruntDex = rng.Next(EffortSounds.Length);
				AudioEffectsManager.PlaySound(EffortSounds[gruntDex]);
			}
			if (FacingDirection != direction)
			{
				IBlock nextFloor = BlockManager.GetBlockNear(transform.position + direction + Vector3.down);
				if (nextFloor != null)
				{
					DoPushPull(direction);
				}
				else
				{
					CurrentMoveState = State.Idle;
					Sidle(direction);
				}
			}
			//TODO the idea here is to lock user input for longer for heavy blocks but this isn't really the right calculation
			return 0.3f * blockInQuestion.WeightFactor;
		}
		Serializer.DeleteLastUndo();
		return 0f;
	}

	public override float ApplyInput(List<string> inputNames, Dictionary<string, string> parameters = null)
	{
		if (CurrentMoveState != State.Idle && CurrentMoveState != State.Sidle)
			return 0f;
		if (Position.y % 1 != 0)
			return 0f;

		float expectedTime = 0f;
		bool pushpull = false;
		if (inputNames.Contains("Grab"))
		{
			pushpull = true;
		}
		if (inputNames.Contains("Up"))
		{
			return QueueMove(Vector3.forward, pushpull);
		}
		else if (inputNames.Contains("Down"))
		{
			return QueueMove(Vector3.back, pushpull);
		}
		else if (inputNames.Contains("Left"))
		{
			return QueueMove(Vector3.left, pushpull);
		}
		else if (inputNames.Contains("Right"))
		{
			return QueueMove(Vector3.right, pushpull);
		}
		else if(inputNames.Contains("Item"))
		{
			if (UseItem())
				return 0.8f;
			return 0f;
		}

		return expectedTime;
	}

	public override Dictionary<string, string> Properties
	{
		get
		{
			Dictionary<string, string> ret = base.Properties;
			ret.Add("Walk Time", WalkTime.ToString());
			return ret;
		}
		set
		{
			base.Properties = value;
			if (value.ContainsKey("Walk Time"))
				WalkTime = int.Parse("Walk Time");
		}
	}
}
