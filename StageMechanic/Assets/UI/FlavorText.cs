﻿public static class FlavorText
{
	public static string[] Entries = new string[25];

	//Thanks to ww.brainyquote.com for these quotes
	static FlavorText()
	{
		Entries[0] =
			"Never give up on what you really want to do.\n" + 
			"The person with big dreams is more powerful\n" +
			"than one with all the facts.\n" +
			"\n" +
			"–- Albert Einstein";

		Entries[1] =
			"The quest for certainty blocks the search for meaning.\n" +
			"Uncertainty is the very condition to impel man to unfold his powers.\n" +
			"\n" +
			"--Erich Fromm";

		Entries[2] =
			"In the end, it all comes to choices to turn stumbling blocks into stepping stones.\n" +
			"\n" +
			"--Amber Frey";

		Entries[3] =
			"When I am on the blocks, I don't care who you are - I will always try and beat you.\n" +
			"\n" +
			"--Chad le Clos";

		Entries[4] =
			"Everyone has something that blocks us from the full experience and expression of our nobility.\n" +
			"\n" +
			"--Iyanla Vanzant";

		Entries[5] =
			"Winning doesn’t always mean being first. Winning means you’re doing\n" +
			"better than you’ve done before.\n" +
			"\n" +
			"--Bonnie Blair";

		Entries[6] =
			"Whenever a toddler sees a pile of blocks, he wants to tear it down.\n" +
			"\n" +
			"--J. J. Abrams";

		Entries[7] =
			"You’re braver than you believe, and stronger than you seem, and smarter than you think.\n" +
			"\n" +
			"--A.A. Milne/Christopher Robin";

		Entries[8] =
			"Look at kids playing with blocks. I think it's in everyone's DNA to want to be a builder.\n" +
			"\n" +
			"--Dan Phillips";

		Entries[9] =
			"My mind is in another planet behind the blocks.\n" +
			"Sometimes I'm up in the blocks, and I'm like, 'What am I doing here?'\n" +
			"I'm just not trying to think too much.\n" +
			"\n" +
			"--Cesar Cielo";

		Entries[10] =
			"Every block of stone has a statue inside it and it is the task of the sculptor to discover it.\n" +
			"\n" +
			"--Michelangelo";

		Entries[11] =
			"The drops of rain make a hole in the stone, not by violence, but by oft falling.\n" +
			"\n" +
			"--Lucretius";

		Entries[12] =
			"Don’t just read the easy stuff. You may be entertained by it, but you will never grow from it.\n" +
			"\n" +
			"--Jim Rohn";

		Entries[13] =
			"Nothing is built on stone; all is built on sand, but we must build as if the sand were stone.\n" +
			"\n" +
			"--Jorge Luis Borges";

		Entries[14] =
			"Love doesn't just sit there, like a stone; it has to be made, like bread, remade all the time, made new.\n" +
			"\n" +
			"--Ursula K. Le Guin";

		Entries[15] =
			"Each of us is carving a stone, erecting a column, or cutting a piece of stained glass in the construction of something much bigger than ourselves.\n" +
			"\n" +
			"--Adrienne Clarkson";

		Entries[16] =
			"The observer, when he seems to himself to be observing a stone, is really, if physics is to be believed, observing the effects of the stone upon himself.\n" +
			"\n" +
			"--Bertrand Russell";

		Entries[17] =
			"The past is a stepping stone, not a millstone.\n" +
			"\n" +
			"--Robert Plant";

		Entries[18] =
			"Why fit in when you were born to stand out?\n" +
			"\n" +
			"--Dr. Seuss";

		Entries[19] =
			"Stone walls do not a prison make, nor iron bars a cage.\n" +
			"\n" +
			"--Richard Lovelace";

		Entries[20] =
			"Most rules that you think are written in stone are just societal. You can change the game and really reach for the stars and make the world a better place.\n" +
			"\n" +
			"--Sebastian Thrun";

		Entries[21] =
			"The reason why the stone is red is its iron content, which is also why our blood is red.\n" +
			"\n" +
			"--Andy Goldsworthy";

		Entries[22] =
			"Nothing is particularly hard if you break it down into small jobs.\n" +
			"\n" +
			"--Henry Ford";

		Entries[23] =
			"You must be the change you wish to see in the world.\n" +
			"\n" +
			"--Mahatma Gandhi";

		Entries[24] =
			"Anger is as a stone cast into a wasp's nest.\n" +
			"\n" +
			"--Pope Paul VI";
	}
	
}
