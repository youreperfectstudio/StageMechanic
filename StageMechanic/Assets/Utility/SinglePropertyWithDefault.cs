﻿/*  
 * Copyright (C) 2018 You're Perfect Studio. All rights reserved.  
 * Licensed under the BSD 3-Clause License.
 * See LICENSE file in the project root for full license information.
 * See CONTRIBUTORS file in the project root for full list of contributors.
 */
using System;
using UnityEngine;

public class SinglePropertyWithDefault : MonoBehaviour {

    public string PropertyName;
    public string PropertyDefaultValue;
    public Type PropertyType;
}
